package com.zen3.task;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.zen3.retrofit.APIClient;
import com.zen3.retrofit.APIInterface;
import com.zen3.utils.ProgressDialog;

/**
 * Created by Sandeep on 05/12/17.
 */

public class BaseActivity extends AppCompatActivity {

    public static final int CONNECTION_OK = 200;
    public APIInterface apiService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        apiService = APIClient.getClientInstance().create(APIInterface.class);
    }

    /**
     * Displays the progress dialog
     */
    protected void showProgressDialog() {
        ProgressDialog.getInstance().displayProgressDialog(this);
    }

    protected void dismissProgressDialog() {
        ProgressDialog.getInstance().dismissProgressDialog();
    }
}
