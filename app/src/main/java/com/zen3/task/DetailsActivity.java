package com.zen3.task;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.zen3.model.Photo;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailsActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.detailImage)
    ImageView detailImage;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.location)
    TextView location;
    @BindView(R.id.postedOn)
    TextView postedOn;
    @BindView(R.id.likes)
    TextView likes;
    @BindView(R.id.comments)
    TextView comments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.details));

        loadData();
    }

    private void loadData() {

        Photo photo = (Photo) getIntent().getSerializableExtra("photo_data");
        String image = photo.getUrl();
        if (!image.isEmpty()) {
            Glide.with(this).load(image).into(detailImage);
        } else {
            Glide.with(this).load("http://www.bsmc.net.au/wp-content/uploads/No-image-available.jpg").into(detailImage);
        }
        Spanned result;
        if (photo.getDescription() != null) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                result = Html.fromHtml(photo.getDescription(), Html.FROM_HTML_MODE_LEGACY);
            } else {
                result = Html.fromHtml(photo.getDescription());
            }
            description.setText(result);
        } else {
            description.setText(getString(R.string.no_decscription));
        }
        if (photo.getLocation() != null) {
            location.setText(photo.getLocation());
        } else {
            location.setText(getString(R.string.no_location));
        }
        postedOn.setText(photo.getPostedOn());
        likes.setText(photo.getLikes() + "");
        comments.setText(photo.getComments() + "");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }
}
