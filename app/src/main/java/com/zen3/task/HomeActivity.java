package com.zen3.task;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.zen3.adapters.ToggleViewAdapter;
import com.zen3.interfaces.OnItemClickListener;
import com.zen3.interfaces.OnLoadMoreListener;
import com.zen3.model.Photo;
import com.zen3.model.Photos;
import com.zen3.utils.CommonMethods;
import com.zen3.utils.Constants;
import com.zen3.utils.PreferenceConnector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends BaseActivity implements View.OnClickListener, OnItemClickListener, OnLoadMoreListener {


    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private ImageButton toggle;
    private Photos photos;
    private boolean isList = true;
    private int totalPages;
    private int pageNumber = 1;
    private List<Photo> photosList = new ArrayList<>();
    private ToggleViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        TextView title = toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.home));
        toggle = toolbar.findViewById(R.id.toggle);
        toggle.setVisibility(View.VISIBLE);
        toggle.setOnClickListener(this);
        if (CommonMethods.isOnline(this)) {
            loadData(pageNumber);
        } else {
            CommonMethods.displayToast(this, getString(R.string.no_internet));
        }
    }

    private void loadData(int pageNumber) {

        showProgressDialog();
        HashMap<String, String> map = new HashMap<>();
        String key = PreferenceConnector.readString(this, PreferenceConnector.CONSUMER_KEY, getString(R.string.px_consumer_key));
        map.put(Constants.CONSUMER_KEY, key);
        map.put(Constants.IMAGE_SIZE, "4");
        map.put(Constants.PAGE, pageNumber + "");
        apiService.getPhotos(map).enqueue(new Callback<Photos>() {
            @Override
            public void onResponse(Call<Photos> call, Response<Photos> response) {
                dismissProgressDialog();
                photos = response.body();
                photosList.addAll(photos.getPhotos());
                if (totalPages == 0) {
                    totalPages = photos.getTotalPages();
                    loadRecycler();
                } else {
                    adapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<Photos> call, Throwable t) {
                dismissProgressDialog();
            }
        });
    }

    private void loadRecycler() {

        adapter = new ToggleViewAdapter(this, photosList);
        final RecyclerView.LayoutManager layoutManager;
        if (isList) {
            layoutManager = new LinearLayoutManager(this);
        } else {
            layoutManager = new GridLayoutManager(this, 2);
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.toggle:
                if (isList) {
                    toggle.setImageResource(R.drawable.ic_list);
                    isList = false;
                    loadRecycler();
                } else {
                    toggle.setImageResource(R.drawable.ic_grid);
                    isList = true;
                    loadRecycler();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void onItemClick(int position) {
        Photo photo = photosList.get(position);
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("photo_data", photo);
        startActivity(intent);
    }

    @Override
    public void onLoadMore() {
        if (pageNumber <= totalPages) {
            if (CommonMethods.isOnline(this)) {
                loadData(++pageNumber);
            } else {
                CommonMethods.displayToast(this, getString(R.string.no_internet));
            }
        } else {
            CommonMethods.displayOKDialogue(this, getString(R.string.no_more_data));
        }
    }
}
