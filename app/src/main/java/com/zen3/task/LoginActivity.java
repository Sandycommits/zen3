package com.zen3.task;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fivehundredpx.api.FiveHundredException;
import com.fivehundredpx.api.auth.AccessToken;
import com.fivehundredpx.api.tasks.XAuth500pxTask;
import com.zen3.utils.CommonMethods;
import com.zen3.utils.Constants;
import com.zen3.utils.PreferenceConnector;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity implements XAuth500pxTask.Delegate {

    @BindView(R.id.userName)
    EditText userName;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.login)
    Button login;
    @BindView(R.id.signUp)
    Button signUp;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        TextView title = toolbar.findViewById(R.id.title);
        title.setText(getString(R.string.login));
    }

    @OnClick({R.id.login, R.id.signUp})
    public void onViewClicks(View view) {

        int id = view.getId();

        switch (id) {
            case R.id.login:
                boolean allFieldsOK = CommonMethods.validateEditTextFields(new EditText[]{userName, password});
                if (allFieldsOK) {
                    performLogin();
                } else {
                    CommonMethods.displayToast(this, getString(R.string.fields_required));
                }
                break;
            case R.id.signUp:
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(Constants.SIGN_UP));
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private void performLogin() {
        if (CommonMethods.isOnline(this)) {
            showProgressDialog();
            XAuth500pxTask loginTask = new XAuth500pxTask(LoginActivity.this);
            String key = getString(R.string.px_consumer_key);
            String secret = getString(R.string.px_consumer_secret);
            loginTask.execute(key, secret, userName
                    .getText().toString(), password.getText()
                    .toString());
        } else {
            CommonMethods.displayToast(this, getString(R.string.no_internet));
        }
    }

    @Override
    public void onSuccess(AccessToken result) {
        dismissProgressDialog();
        PreferenceConnector.writeString(this, PreferenceConnector.CONSUMER_KEY, getString(R.string.px_consumer_key));
        PreferenceConnector.writeString(this, PreferenceConnector.CONSUMER_SECRET, getString(R.string.px_consumer_secret));
        PreferenceConnector.writeString(this, PreferenceConnector.ACCESS_TOKEN, result.getToken());

        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        this.finish();
    }

    @Override
    public void onFail(FiveHundredException e) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dismissProgressDialog();
                CommonMethods.displayOKDialogue(LoginActivity.this, getString(R.string.please_chceck));
            }
        });
    }
}
