package com.zen3.interfaces;

/**
 * Created by Sandeep on 05/12/17.
 */

public interface OnItemClickListener {

    void onItemClick(int position);
}
