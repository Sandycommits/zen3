package com.zen3.interfaces;

/**
 * Created by Sandeep on 06/12/17.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}
