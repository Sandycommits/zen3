package com.zen3.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.zen3.interfaces.OnItemClickListener;
import com.zen3.interfaces.OnLoadMoreListener;
import com.zen3.model.Photo;
import com.zen3.task.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sandeep on 05/12/17.
 */

public class ToggleViewAdapter extends RecyclerView.Adapter<ToggleViewAdapter.ViewHolder> {

    private List<Photo> photos = new ArrayList<>();
    private LayoutInflater mInflater;
    private OnItemClickListener mClickListener;
    private Context context;
    private OnLoadMoreListener moreListener;

    public ToggleViewAdapter(Context context, List<Photo> photos) {
        this.mInflater = LayoutInflater.from(context);
        this.photos = photos;
        mClickListener = (OnItemClickListener) context;
        this.moreListener = (OnLoadMoreListener) context;
        this.context = context;
    }

    @Override
    public ToggleViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recycler_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ToggleViewAdapter.ViewHolder holder, int position) {

        Photo photo = photos.get(position);
        String image = photo.getUrl();
        if (position == photos.size() - 1) {
            moreListener.onLoadMore();

        }
        if (!image.isEmpty()) {
            Glide.with(context).load(image).into(holder.imageView);
        } else {
            Glide.with(context).load("http://www.bsmc.net.au/wp-content/uploads/No-image-available.jpg").into(holder.imageView);
        }
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }

    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        ImageView imageView;

        ViewHolder(View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.image);
            imageView.setOnClickListener(this);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null)
                mClickListener.onItemClick(getAdapterPosition());
        }
    }
}
