package com.zen3.utils;

import android.content.Context;

/**
 * Created by Sandeep on 05/12/17.
 */

public class ProgressDialog {

    private static ProgressDialog progressDialog;
    private android.app.ProgressDialog dialog;

    private ProgressDialog() {

    }

    public static ProgressDialog getInstance() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog();
        }

        return progressDialog;
    }

    public void displayProgressDialog(Context ctx) {
        dialog = android.app.ProgressDialog.show(ctx,
                "Status",
                "Loading, Please wait...");
        dialog.setCancelable(false);
    }

    public void dismissProgressDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
