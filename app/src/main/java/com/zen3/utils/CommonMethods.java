package com.zen3.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by Sandeep on 05/12/17.
 */

public class CommonMethods {

    private static final String TASK = "TASK";

    /**
     * Prints log statement
     *
     * @param msg
     */
    public static void getLog(String msg) {
        Log.i(TASK, msg);
    }

    /**
     * displays the toast message
     *
     * @param ctx
     * @param msg
     */
    public static void displayToast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Check edit text fields are empty or not
     *
     * @param fields
     * @return
     */
    public static boolean validateEditTextFields(EditText[] fields) {
        for (EditText currentField : fields) {
            if (currentField.getText().toString().length() <= 0) {
                return false;
            }
        }
        return true;
    }

    /*
    * Display Sample dialogue with "OK" button
    * */
    public static void displayOKDialogue(Context context, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //do things
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }
}
