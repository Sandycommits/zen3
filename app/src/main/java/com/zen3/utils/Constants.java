package com.zen3.utils;

/**
 * Created by Sandeep on 05/12/17.
 */

public class Constants {

    public static final String BASE_URL = "https://api.500px.com/v1/";
    public static final String CONSUMER_KEY = "consumer_key";
    public static final String IMAGE_SIZE = "image_size";
    public static final String PAGE = "page";
    public static final String SIGN_UP = "https://500px.com/signup";
}
