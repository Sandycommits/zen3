package com.zen3.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Sandeep on 05/12/17.
 */

public class Photos {

    @SerializedName("photos")
    @Expose
    private List<Photo> photos;

    @SerializedName("total_pages")
    @Expose
    private int totalPages;

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}
