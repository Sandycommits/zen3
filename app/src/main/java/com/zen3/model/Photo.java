package com.zen3.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Sandeep on 05/12/17.
 */

public class Photo implements Serializable {

    @SerializedName("image_url")
    @Expose
    private String url = "";

    @SerializedName("description")
    @Expose
    private String description = "";

    @SerializedName("location")
    @Expose
    private String location = "";

    @SerializedName("created_at")
    @Expose
    private String postedOn = "";

    @SerializedName("votes_count")
    @Expose
    private int likes = 0;

    @SerializedName("comments_count")
    @Expose
    private int comments = 0;

    public String getUrl() {
        return url;
    }

    public String getDescription() {
        return description;
    }

    public String getLocation() {
        return location;
    }

    public String getPostedOn() {
        return postedOn;
    }

    public int getLikes() {
        return likes;
    }

    public int getComments() {
        return comments;
    }
}
