package com.zen3.retrofit;

import com.zen3.model.Photos;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by Sandeep on 05/12/17.
 */

public interface APIInterface {

    /**
     * Login request
     *
     * @return
     */
    @GET("photos")
    Call<Photos> getPhotos(@QueryMap Map<String, String> params);

}
